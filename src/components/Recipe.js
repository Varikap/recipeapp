import React from 'react';
import style from './recipeStyle.module.css'

const Recipe = ({title, calories, image, ing}) => {
    return (
        <div className={style.recipe}>
            <h1>{title}</h1>
            <ol>
                {ing.map(i => 
                    <li key="i.text">{i.text}</li>
                )}
            </ol>
            <p>{calories}</p>
            <img src={image} alt={title} />
        </div>
    );
}

export default Recipe;